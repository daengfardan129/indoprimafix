import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:myapp/page-1/home.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:myapp/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:myapp/api_service.dart';

class Setting extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Setting();
  }
}

class _Setting extends State<Setting> {
  var macData = Hive.box("macDataDewasa");
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  List<BluetoothService> bluetoothService = [];
  Map<String, List<int>> notifyDatas = {};
  FlutterBluePlus flutterBlue = FlutterBluePlus.instance;
  List<ScanResult> scanResultList = [];
  bool _isScanning = false;

  int counter = 0;
  String clickedButton = "";
  String deviceVal = "";

  String bodyHeight = "-";
  String bodyWeight = "-";
  String bodyHead = "-";
  String sender = "-";
  String typescan = "";
  int isHeightAvailable = 0;
  int isWeightAvailable = 0;
  int isHeadAvailable = 0;

  onScan(String mac_id, type) async {
    int deviceAvailable = 0;

    // typescan = "height";
    if (!_isScanning) {
      scanResultList.clear();
      flutterBlue.startScan(timeout: Duration(seconds: 1));
      flutterBlue.scanResults.listen((results) {
        scanResultList = results;
        print(results);
        if (!mounted) return;
        setState(() {});
        for (int i = 0; i < results.length; i++) {
          if (mac_id.compareTo(results[i].device.id.id) == 0) {
            deviceAvailable = 1;
            flutterBlue.stopScan();
            print("Mac Address Found");
          } else {
            print("Mac Address not Found");
          }
        }
      });
      Future.delayed(Duration(seconds: 1), () {
        // <-- Delay here
        if (!mounted) return;
        setState(() {
          if (deviceAvailable == 1) {
            print("Mac Address Success");
            print("MAC Addres $type is available");
            showDialog(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Success'),
                content: Text("MAC Addres $type is available"),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'OK'),
                    child: const Text('OK'),
                  ),
                ],
              ),
            );
          } else {
            print("Mac Address Eror");
            print("MAC Addres $type not available");
            showDialog(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text("MAC Addres $type not available"),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'OK'),
                    child: const Text('OK'),
                  ),
                ],
              ),
            );
          }
        });
      });
    } else {
      flutterBlue.stopScan();
    }
  }

  Stream _bluetoothList() async* {
    await Future<void>.delayed(const Duration(seconds: 3));
  }

  void initBle() {
    // BLE 스캔 상태 얻기 위한 리스너
    flutterBlue.isScanning.listen((isScanning) {
      _isScanning = isScanning;
      if (!mounted) return;
      setState(() {});
    });
  }

  @override
  initState() {
    super.initState();
    // 블루투스 초기화
    initBle();
  }

  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Mac Addrress Dewasa',
          style: SafeGoogleFont('Nunito', fontWeight: FontWeight.w700),
        ),
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        // profiledvG (5:1108)
        // width: double.infinity,
        // decoration: BoxDecoration(
        //   gradient: LinearGradient(
        //     begin: Alignment(-0.834, -0.431),
        //     end: Alignment(4.063, 3.18),
        //     colors: <Color>[Color(0xfff6f6f6), Color(0xff7fcce2)],
        //     stops: <double>[0, 1],
        //   ),
        // ),
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.all(12.0),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 0 * fem, 30 * fem),
                      width: 80 * fem,
                      height: 80 * fem,
                      decoration: BoxDecoration(
                        color: Color.fromARGB(122, 24, 65, 108),
                        borderRadius: BorderRadius.circular(12 * fem),
                      ),
                      child: Center(
                        // vectorgfa (5:1124)
                        child: SizedBox(
                          width: 50 * fem,
                          height: 50 * fem,
                          child: Icon(CupertinoIcons.gear,
                              color: Colors.blue, size: 26.0),
                        ),
                      ),
                    ),
                    Container(
                      // frame65okC (5:1114)
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            padding: EdgeInsets.fromLTRB(
                                12 * fem, 10 * fem, 11 * fem, 19 * fem),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(12 * fem),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x0a000000),
                                  offset: Offset(0 * fem, 4 * fem),
                                  blurRadius: 8.5 * fem,
                                ),
                              ],
                            ),
                            child: Container(
                              padding: EdgeInsets.only(right: 5.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: 300 * ffem,
                                    child: TextFormField(
                                      initialValue: macData.get("macHeight"),
                                      onChanged: (value) {
                                        if (!mounted) return;
                                        setState(() {
                                          macData.put("macHeight", value);
                                        });
                                      },
                                      decoration: InputDecoration(
                                          labelText: "Mac Address Height",
                                          hintText: 'Input Mac Address'),
                                    ),
                                  ),
                                  Spacer(),
                                  TextButton(
                                    // onPressed: HScan(macData.get("macHeight")),
                                    onPressed: () {
                                      onScan(
                                          macData.get("macHeight"), "Height");
                                    },
                                    child: Text(
                                      "Connect",
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 22 * fem,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(
                                12 * fem, 20 * fem, 11 * fem, 19 * fem),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(12 * fem),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x0a000000),
                                  offset: Offset(0 * fem, 4 * fem),
                                  blurRadius: 8.5 * fem,
                                ),
                              ],
                            ),
                            child: Container(
                              padding: EdgeInsets.only(right: 5.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: 300 * ffem,
                                    child: TextFormField(
                                      initialValue: macData.get("macWeight"),
                                      onChanged: (value) {
                                        if (!mounted) return;
                                        setState(() {
                                          macData.put("macWeight", value);
                                        });
                                      },
                                      decoration: InputDecoration(
                                          labelText: "Mac Address Weight",
                                          hintText: 'Input Mac Address'),
                                    ),
                                  ),
                                  Spacer(),
                                  TextButton(
                                    // onPressed: HScan(macData.get("macHeight")),
                                    onPressed: () {
                                      onScan(
                                          macData.get("macWeight"), "Weight");
                                    },
                                    child: Text(
                                      "Connect",
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 22 * fem,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(
                                12 * fem, 20 * fem, 11 * fem, 19 * fem),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(12 * fem),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x0a000000),
                                  offset: Offset(0 * fem, 4 * fem),
                                  blurRadius: 8.5 * fem,
                                ),
                              ],
                            ),
                            child: Container(
                              padding: EdgeInsets.only(right: 5.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: 300 * ffem,
                                    child: TextFormField(
                                      initialValue: macData.get("macHead"),
                                      onChanged: (value) {
                                        if (!mounted) return;
                                        setState(() {
                                          macData.put("macHead", value);
                                        });
                                      },
                                      decoration: InputDecoration(
                                          labelText: "Mac Address Head",
                                          hintText: 'Input Mac Address'),
                                    ),
                                  ),
                                  Spacer(),
                                  TextButton(
                                    // onPressed: HScan(macData.get("macHeight")),
                                    onPressed: () {
                                      onScan(macData.get("macHead"), "Head");
                                    },
                                    child: Text(
                                      "Connect",
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
