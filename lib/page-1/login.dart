import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:hive/hive.dart';
import 'package:myapp/utils.dart';

import 'package:myapp/api_service.dart';
import 'home.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Login();
  }
}

class _Login extends State<Login> {
  var userData = Hive.box("userData");
  String email = "";
  String password = "";
  void onChangePassword(value) {
    setState(() {
      password = value;
    });
  }

  void onChangeEmail(value) {
    setState(() {
      email = value;
    });
  }

  Future<void> onLogin() async {
    // final params = {
    //   "db": "dev_indoprima",
    //   "login": email,
    //   "password": password
    // };
    // final formData = {"jsonrpc": "2.0", "method": "call", "params": params};
    // final data = await ApiService().login(formData);
    if (password != "" && email != "") {
      userData.put("name", email);
      context.goNamed('patienData');
    } else {
      print("email and password can't empty");
    }
  }

  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: Container(
            // iphone13promax4tba (5:54)
            padding:
                EdgeInsets.fromLTRB(34 * fem, 56 * fem, 34 * fem, 200.73 * fem),
            width: double.infinity,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-0.834, -0.431),
                end: Alignment.centerRight,
                colors: <Color>[Color(0xffffffff), Color(0xffc3dfe7)],
                stops: <double>[0, 1],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  // loginaccountAb2 (45:500)
                  margin:
                      EdgeInsets.fromLTRB(0 * fem, 0 * fem, 1 * fem, 121 * fem),
                  child: Text(
                    'Login Account',
                    style: SafeGoogleFont(
                      'Nunito',
                      fontSize: 24 * ffem,
                      fontWeight: FontWeight.w800,
                      height: 1.3625 * ffem / fem,
                      color: Color(0xff18416c),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(
                      27 * fem, 0 * fem, 28 * fem, 156.17 * fem),
                  width: double.infinity,
                  height: 78.83 * fem,
                  child: AspectRatio(
                    aspectRatio: 16 / 4,
                    child: Image.asset(
                      'assets/page-1/images/IndoPrima.png',
                    ),
                  ),
                ),
                Container(
                  // hellowelcomebacktoouraccountQ2 (5:69)
                  margin: EdgeInsets.fromLTRB(
                      0 * fem, 0 * fem, 107 * fem, 16 * fem),
                  child: Text(
                    'Hello , welcome back to our account !',
                    style: SafeGoogleFont(
                      'Nunito',
                      fontSize: 14.5341615677 * ffem,
                      fontWeight: FontWeight.w500,
                      height: 1.3625 * ffem / fem,
                      color: Color(0xff626262),
                    ),
                  ),
                ),
                Container(
                  // frame53fjN (5:487)
                  margin: EdgeInsets.fromLTRB(
                      0 * fem, 0 * fem, 3.35 * fem, 10.99 * fem),
                  width: 356.65 * fem,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        // textfieldusernameNtg (5:485)
                        margin: EdgeInsets.fromLTRB(
                            0 * fem, 0 * fem, 0 * fem, 14 * fem),
                        width: double.infinity,
                        child: TextField(
                          onChanged: (value) => onChangeEmail(value),
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Email address',
                              hintText: 'Enter valid mail id as abc@gmail.com'),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        child: TextField(
                          onChanged: (value) => onChangePassword(value),
                          obscureText: true,
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Password',
                              hintText: 'Enter your secure password'),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  // forgotpassword3dS (5:60)
                  margin: EdgeInsets.fromLTRB(
                      221.7 * fem, 0 * fem, 0 * fem, 35.28 * fem),
                  child: Text(
                    'Forgot Password ?',
                    style: SafeGoogleFont(
                      'Outfit',
                      fontSize: 14.7287425995 * ffem,
                      fontWeight: FontWeight.w600,
                      height: 1.26 * ffem / fem,
                      color: Color(0xff444343),
                    ),
                  ),
                ),
                Container(
                  // group40YaC (5:61)
                  margin: EdgeInsets.fromLTRB(
                      0 * fem, 0 * fem, 0 * fem, 27.78 * fem),
                  width: double.infinity,
                  height: 61.49 * fem,
                  decoration: BoxDecoration(
                    color: Color(0xff0086c9),
                    borderRadius: BorderRadius.circular(13.4161491394 * fem),
                  ),
                  child: InkWell(
                      onTap: () => onLogin(),
                      child: Center(
                        child: Text(
                          'Login',
                          style: SafeGoogleFont(
                            'Nunito',
                            fontSize: 20.1242237091 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.3625 * ffem / fem,
                            color: Color(0xffffffff),
                          ),
                        ),
                      )),
                ),
                Container(
                  // notregisteryetcreateaccountc4G (5:70)
                  margin: EdgeInsets.fromLTRB(
                      0 * fem, 0 * fem, 15.02 * fem, 0 * fem),
                  child: RichText(
                    text: TextSpan(
                      style: SafeGoogleFont(
                        'Poppins',
                        fontSize: 14.5341615677 * ffem,
                        fontWeight: FontWeight.w400,
                        height: 1.5000000328 * ffem / fem,
                        color: Color(0xff626262),
                      ),
                      children: [
                        TextSpan(
                          text: 'Not register yet ?',
                          style: SafeGoogleFont(
                            'Nunito',
                            fontSize: 14.5341615677 * ffem,
                            fontWeight: FontWeight.w600,
                            height: 1.3625 * ffem / fem,
                            color: Color(0xff626262),
                          ),
                        ),
                        TextSpan(
                          text: ' ',
                          style: SafeGoogleFont(
                            'Nunito',
                            fontSize: 14.5341615677 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.3625 * ffem / fem,
                            color: Color(0xff626262),
                          ),
                        ),
                        TextSpan(
                          text: 'Create Account',
                          style: SafeGoogleFont(
                            'Nunito',
                            fontSize: 14.5341615677 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.3625 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
