import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:hive/hive.dart';
import 'package:myapp/utils.dart';

class NikScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _NikScreen();
  }
}

class _NikScreen extends State<NikScreen> {
  var patienData = Hive.box("patienData");
  String nik = "3201901302940001";
  String name = "John Doe";
  String gender = "Male";
  String age = "25";
  String address = "";
  List<String> genderList = <String>['Male', 'Female'];

  TextEditingController nikController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController ageController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    nikController.text = nik;
    nameController.text = name;
    ageController.text = age;
    super.initState();
  }

  void onSubmit() {
    if (nik != "" && name != "" && gender != "" && age != "") {
      patienData.put("gender", gender);
      context.goNamed('home');
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Error'),
          content: const Text("Data can't empty!"),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'OK'),
              child: const Text('OK'),
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(12.0),
          child: SizedBox(
            width: double.infinity,
            child: SizedBox(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // maskgroupMm2 (43:1646)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 0 * fem, 16 * fem),
                    width: 260 * fem,
                    height: 130 * fem,
                    child: Image.asset(
                      'assets/page-1/images/mask-group.png',
                      width: 360 * fem,
                      height: 224.37 * fem,
                    ),
                  ),
                  Container(
                    // frame53fjN (5:487)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 0 * fem, 10.99 * fem),
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          // textfieldusernameNtg (5:485)
                          margin: EdgeInsets.fromLTRB(
                              0 * fem, 0 * fem, 0 * fem, 14 * fem),
                          width: double.infinity,
                          child: TextField(
                            controller: nikController,
                            onChanged: (value) {
                              setState(() {
                                patienData.put("nik", value);
                                nik = value.toString();
                              });
                            },
                            decoration: InputDecoration(
                                border: OutlineInputBorder(gapPadding: 1.0),
                                labelText: 'NIK',
                                hintText: 'Enter valid NIK'),
                          ),
                        ),
                        Container(
                          // textfieldusernameNtg (5:485)
                          margin: EdgeInsets.fromLTRB(
                              0 * fem, 0 * fem, 0 * fem, 14 * fem),
                          width: double.infinity,
                          child: TextField(
                            controller: nameController,
                            onChanged: (value) {
                              setState(() {
                                patienData.put("name", value);
                                name = value.toString();
                              });
                            },
                            decoration: InputDecoration(
                                border: OutlineInputBorder(gapPadding: 1.0),
                                labelText: 'Nama',
                                hintText: 'Enter patien name'),
                          ),
                        ),
                        Container(
                          // textfieldusernameNtg (5:485)
                          margin: EdgeInsets.fromLTRB(
                              0 * fem, 0 * fem, 0 * fem, 14 * fem),
                          width: double.infinity,
                          child: TextField(
                            controller: ageController,
                            onChanged: (value) {
                              setState(() {
                                patienData.put("age", value);
                                age = value;
                              });
                            },
                            decoration: InputDecoration(
                                border: OutlineInputBorder(gapPadding: 1.0),
                                labelText: 'Age',
                                hintText: 'Enter patien age'),
                          ),
                        ),
                        Container(
                          // textfieldusernameNtg (5:485)
                          margin: EdgeInsets.fromLTRB(
                              0 * fem, 0 * fem, 0 * fem, 14 * fem),
                          width: double.infinity,
                          child: Container(
                            padding: EdgeInsets.only(
                                top: 5.0, bottom: 5.0, left: 10.0),
                            decoration: BoxDecoration(
                                border: Border.all(
                                    width: 1.0,
                                    color: Color.fromARGB(255, 141, 136, 136)),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4.0))),
                            child: DropdownButton<String>(
                              value: gender,
                              elevation: 16,
                              isExpanded: true,
                              onChanged: (String? value) {
                                // This is called when the user selects an item.
                                setState(() {
                                  gender = value!;
                                });
                              },
                              items: genderList.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                        Container(
                          // textfieldusernameNtg (5:485)
                          margin: EdgeInsets.fromLTRB(
                              0 * fem, 0 * fem, 0 * fem, 14 * fem),
                          width: double.infinity,
                          child: TextField(
                            onChanged: (value) {
                              setState(() {
                                patienData.put("address", value);
                                address = value.toString();
                              });
                            },
                            keyboardType: TextInputType.multiline,
                            maxLines: 4,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(gapPadding: 1.0),
                                labelText: 'Address',
                                hintText: 'Enter patien address'),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 61.49 * fem,
                    decoration: BoxDecoration(
                      color: Color(0xff0086c9),
                      borderRadius: BorderRadius.circular(13.4161491394 * fem),
                    ),
                    child: InkWell(
                        onTap: () => onSubmit(),
                        child: Center(
                          child: Text(
                            'Submit',
                            style: SafeGoogleFont(
                              'Nunito',
                              fontSize: 20.1242237091 * ffem,
                              fontWeight: FontWeight.w700,
                              height: 1.3625 * ffem / fem,
                              color: Color(0xffffffff),
                            ),
                          ),
                        )),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
