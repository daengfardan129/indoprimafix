import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'home.dart';
import 'package:myapp/page-1/setting.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';
import 'package:hive/hive.dart';
import 'package:myapp/page-1/home.dart';
import 'package:myapp/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:myapp/api_service.dart';

class BodyMeasurementsChild extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BodyMeasurementsChild();
  }
}

class NumberCreator {
  NumberCreator() {
    Timer.periodic(Duration(seconds: 5), (timer) {
      //add count to stream
      _controller.sink.add(_count);
      _count++;
    });
  }
  var _count = 1;
  final _controller = StreamController<int>();

  Stream<int> get stream => _controller.stream;
}

class _BodyMeasurementsChild extends State<BodyMeasurementsChild> {
  var macData = Hive.box("macDataAnak");
  var patienData = Hive.box("patienData");
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  DeviceInfoPlugin deviceInfo1 = DeviceInfoPlugin();
  DeviceInfoPlugin deviceInfo2 = DeviceInfoPlugin();
  List<BluetoothService> bluetoothService = [];
  List<BluetoothService> bluetoothService1 = [];
  List<BluetoothService> bluetoothService2 = [];
  Map<String, List<int>> notifyDatas = {};
  Map<String, List<int>> notifyDatas1 = {};
  Map<String, List<int>> notifyDatas2 = {};
  FlutterBluePlus flutterBlue = FlutterBluePlus.instance;
  FlutterBluePlus flutterBlue1 = FlutterBluePlus.instance;
  FlutterBluePlus flutterBlue2 = FlutterBluePlus.instance;
  List<ScanResult> scanResultList = [];
  List<ScanResult> scanResultList1 = [];
  List<ScanResult> scanResultList2 = [];
  bool _isScanning = false;
  bool _isScanning1 = false;
  bool _isScanning2 = false;

  int counter = 0;
  String clickedButton = "";
  String deviceVal = "";

  String bodyHeight = "-";
  String bodyWeight = "-";
  String bodyHead = "-";
  String sender = "-";
  String typescan = "";
  int isScannedHeight = 0;
  int StopDeviceHeight = 0;
  int HeightToSend = 0;
  int StopDeviceWeight = 0;
  int WeightToSend = 0;
  int StopDeviceHead = 0;
  int HeadToSend = 0;
  int done_Connect = 0;
  int done_Connect1 = 0;
  int done_Connect2 = 0;
  void onSubmit() async {
    if (Platform.isAndroid) {
      AndroidDeviceInfo? androidInfo = await deviceInfo.androidInfo;
      sender = "${androidInfo.brand} ${androidInfo.device}";
    } else if (Platform.isIOS) {
      IosDeviceInfo? iosDeviceInfo = await deviceInfo.iosInfo;
      sender = "${iosDeviceInfo.name}";
    } else {
      sender = "Admin";
    }
    if (bodyHeight != "-" && bodyWeight != "-") {
      final patient = {
        'nik': patienData.get("nik"),
        'name': patienData.get("name"),
        'age': patienData.get("age"),
        'gender': patienData.get("gender"),
      };

      final patientData = {
        'sender': sender,
        'height': bodyHeight,
        'weight': bodyWeight,
        'head_circumference': bodyHead,
      };

      final formData = {
        'patient': patient,
        'patient_data': patientData,
      };

      final data = await ApiService().insertPatientData(formData);
      print("submit Data : ");
      print(data);

      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Home()),
      );
    } else {
      print("err data can't be empty");
    }
  }

  @override
  initState() {
    super.initState();
    // 블루투스 초기화
    initBle();
    // print("mac heigh adalah : " + );
    IScan(macData.get("macHeight"));
    // BConnect("08:B6:1F:3B:57:2A");
  }

  IScan(String mac_id) async {
    StopDeviceHeight = 0;
    if (!_isScanning) {
      scanResultList.clear();
      flutterBlue.startScan(timeout: Duration(seconds: 4));
      flutterBlue.scanResults.listen((results) {
        scanResultList = results;
        if (!mounted) return;
        setState(() {});
        for (int i = 0; i < results.length; i++) {
          // print(deviceMacAddress(results[i]));
          if (mac_id.compareTo(results[i].device.id.id) == 0) {
            // print("hasil ke " + i.toString());
            // print(deviceMacAddress(results[i]));
            // connect(results[i].device);
            if (done_Connect == 0) {
              connect(scanResultList[i].device);
            }
            done_Connect = 1;
          }
        }
      });
    } else {
      flutterBlue.stopScan();
    }
  }

  IScan1(String mac_id) async {
    StopDeviceWeight = 0;
    if (!_isScanning1) {
      scanResultList1.clear();
      flutterBlue1.startScan(timeout: Duration(seconds: 4));
      flutterBlue1.scanResults.listen((results) {
        scanResultList1 = results;
        if (!mounted) return;
        setState(() {});
        for (int i = 0; i < results.length; i++) {
          // print(deviceMacAddress(results[i]));
          if (mac_id.compareTo(results[i].device.id.id) == 0) {
            if (done_Connect1 == 0) {
              connect1(scanResultList1[i].device);
            }
            done_Connect1 = 1;
          }
        }
      });
    } else {
      flutterBlue1.stopScan();
    }
  }

  IScan2(String mac_id) async {
    StopDeviceHead = 0;
    if (!_isScanning2) {
      scanResultList2.clear();
      flutterBlue2.startScan(timeout: Duration(seconds: 4));
      flutterBlue2.scanResults.listen((results) {
        scanResultList2 = results;
        if (!mounted) return;
        setState(() {});
        for (int i = 0; i < results.length; i++) {
          // print(deviceMacAddress(results[i]));
          if (mac_id.compareTo(results[i].device.id.id) == 0) {
            if (done_Connect2 == 0) {
              connect2(scanResultList2[i].device);
            }
            done_Connect2 = 1;
          }
        }
      });
    } else {
      flutterBlue2.stopScan();
    }
  }

  void initBle() {
    // BLE 스캔 상태 얻기 위한 리스너
    flutterBlue.isScanning.listen((isScanning) {
      _isScanning = isScanning;
      if (!mounted) return;
      setState(() {});
    });
  }

  void initBle1() {
    // BLE 스캔 상태 얻기 위한 리스너
    flutterBlue1.isScanning.listen((isScanning) {
      _isScanning1 = isScanning;
      if (!mounted) return;
      setState(() {});
    });
  }

  void initBle2() {
    // BLE 스캔 상태 얻기 위한 리스너
    flutterBlue2.isScanning.listen((isScanning) {
      _isScanning2 = isScanning;
      if (!mounted) return;
      setState(() {});
    });
  }

  WScan() async {
    print("function scan");
    typescan = "weight";
    if (!_isScanning) {
      scanResultList.clear();
      flutterBlue.startScan(timeout: Duration(seconds: 4));
      flutterBlue.scanResults.listen((results) {
        scanResultList = results;
        if (!mounted) return;
        setState(() {});
        print("hasil result");
        print(results);
      });
      print("scan : masuk if");
    } else {
      flutterBlue.stopScan();
      print("scan : masuk else");
    }
  }

  HScan() async {
    typescan = "height";
    print("function scan");
    if (!_isScanning) {
      scanResultList.clear();
      flutterBlue.startScan(timeout: Duration(seconds: 4));
      flutterBlue.scanResults.listen((results) {
        scanResultList = results;
        if (!mounted) return;
        setState(() {});
      });
      isScannedHeight = 1;
    } else {
      flutterBlue.stopScan();
      print("scan : masuk else");
    }
  }

  HdScan() async {
    typescan = "head";
    print("function scan");
    if (!_isScanning) {
      scanResultList.clear();
      flutterBlue.startScan(timeout: Duration(seconds: 4));
      flutterBlue.scanResults.listen((results) {
        scanResultList = results;
        if (!mounted) return;
        setState(() {});
        print("hasil result");
        print(results);
      });
      print("scan : masuk if");
    } else {
      flutterBlue.stopScan();
      print("scan : masuk else");
    }
  }

  Widget deviceSignal(ScanResult r) {
    return Text(r.rssi.toString());
  }

  Widget deviceMacAddress(ScanResult r) {
    return Text(r.device.id.id);
  }

  /* 장치의 명 위젯  */
  Widget deviceName(ScanResult r) {
    String name = '';

    if (r.device.name.isNotEmpty) {
      // device.name에 값이 있다면
      name = r.device.name;
    } else if (r.advertisementData.localName.isNotEmpty) {
      // advertisementData.localName에 값이 있다면
      name = r.advertisementData.localName;
    } else {
      // 둘다 없다면 이름 알 수 없음...
      name = 'N/A';
    }
    return Text(name);
  }

  Widget leading(ScanResult r) {
    return CircleAvatar(
      child: Icon(
        Icons.bluetooth,
        color: Colors.white,
      ),
      backgroundColor: Colors.cyan,
    );
  }

  void onTap(ScanResult r) {
    connect(r.device);
    print('${r.device}');
  }

  // @override
  // void dispose() {
  //   // 상태 리스터 해제
  //   _stateListener?.cancel();
  //   // 연결 해제
  //   disconnect();
  //   super.dispose();
  // }

  Future<bool> connect(device) async {
    Future<bool>? returnValue;
    setState(() {});
    await device
        .connect(autoConnect: false)
        .timeout(Duration(milliseconds: 15000), onTimeout: () {
      returnValue = Future.value(false);
      debugPrint('timeout failed');
    }).then((data) async {
      bluetoothService.clear();
      if (returnValue == null) {
        //returnValue가 null이면 timeout이 발생한 것이 아니므로 연결 성공
        debugPrint('connection successful');
        print('start discover service');
        List<BluetoothService> bleServices = await device.discoverServices();
        if (!mounted) return;
        setState(() {
          bluetoothService = bleServices;
        });
        // 각 속성을 디버그에 출력
        for (BluetoothService service in bleServices) {
          for (BluetoothCharacteristic c in service.characteristics) {
            if (c.properties.notify && c.descriptors.isNotEmpty) {
              for (BluetoothDescriptor d in c.descriptors) {
                if (d.uuid == BluetoothDescriptor.cccd) {
                  print('d.lastValue: ${d.lastValue}');
                }
              }

              if (!c.isNotifying) {
                try {
                  await c.setNotifyValue(true);
                  notifyDatas[c.uuid.toString()] = List.empty();
                  c.value.listen((value) {
                    //print(value.last);
                    //print('${c.uuid}: $value daeng');
                    if (!mounted) return;
                    setState(() {
                      int sum = 0;
                      for (int var_Data in value) {
                        sum += var_Data;
                      }
                      bodyHeight = sum.toString();
                      notifyDatas[c.uuid.toString()] = value;
                      if (StopDeviceHeight == 1) {
                        device.disconnect();
                        HeightToSend = sum;
                        print(HeightToSend);
                      }
                    });
                  });

                  // 설정 후 일정시간 지연
                  await Future.delayed(const Duration(milliseconds: 500));
                } catch (e) {
                  print('error ${c.uuid} $e');
                }
              }
            }
          }
        }
        returnValue = Future.value(true);
      }
    });

    return returnValue ?? Future.value(false);
  }

  Future<bool> connect1(device) async {
    Future<bool>? returnValue;
    setState(() {});

    await device
        .connect(autoConnect: false)
        .timeout(Duration(milliseconds: 15000), onTimeout: () {
      returnValue = Future.value(false);
      debugPrint('timeout failed');
    }).then((data) async {
      bluetoothService1.clear();
      if (returnValue == null) {
        //returnValue가 null이면 timeout이 발생한 것이 아니므로 연결 성공
        debugPrint('connection successful');
        print('start discover service');
        List<BluetoothService> bleServices = await device.discoverServices();
        if (!mounted) return;
        setState(() {
          bluetoothService1 = bleServices;
        });
        // 각 속성을 디버그에 출력
        for (BluetoothService service in bleServices) {
          for (BluetoothCharacteristic c in service.characteristics) {
            if (c.properties.notify && c.descriptors.isNotEmpty) {
              for (BluetoothDescriptor d in c.descriptors) {
                if (d.uuid == BluetoothDescriptor.cccd) {
                  print('d.lastValue: ${d.lastValue}');
                }
              }

              if (!c.isNotifying) {
                try {
                  await c.setNotifyValue(true);
                  notifyDatas1[c.uuid.toString()] = List.empty();
                  c.value.listen((value) {
                    //print(value.last);
                    //print('${c.uuid}: $value daeng');
                    if (!mounted) return;
                    setState(() {
                      int sum = 0;
                      for (int var_Data in value) {
                        sum += var_Data;
                      }
                      bodyWeight = sum.toString();
                      notifyDatas1[c.uuid.toString()] = value;
                      if (StopDeviceWeight == 1) {
                        device.disconnect();
                        WeightToSend = sum;
                        print(WeightToSend);
                      }
                      counter++;
                    });
                  });

                  // 설정 후 일정시간 지연
                  await Future.delayed(const Duration(milliseconds: 500));
                } catch (e) {
                  print('error ${c.uuid} $e');
                }
              }
            }
          }
        }
        returnValue = Future.value(true);
      }
    });

    return returnValue ?? Future.value(false);
  }

  Future<bool> connect2(device) async {
    Future<bool>? returnValue;
    setState(() {});

    await device
        .connect(autoConnect: false)
        .timeout(Duration(milliseconds: 15000), onTimeout: () {
      returnValue = Future.value(false);
      debugPrint('timeout failed');
    }).then((data) async {
      bluetoothService2.clear();
      if (returnValue == null) {
        //returnValue가 null이면 timeout이 발생한 것이 아니므로 연결 성공
        debugPrint('connection successful');
        print('start discover service');
        List<BluetoothService> bleServices = await device.discoverServices();
        if (!mounted) return;
        setState(() {
          bluetoothService2 = bleServices;
        });
        // 각 속성을 디버그에 출력
        for (BluetoothService service in bleServices) {
          for (BluetoothCharacteristic c in service.characteristics) {
            if (c.properties.notify && c.descriptors.isNotEmpty) {
              for (BluetoothDescriptor d in c.descriptors) {
                if (d.uuid == BluetoothDescriptor.cccd) {
                  print('d.lastValue: ${d.lastValue}');
                }
              }

              if (!c.isNotifying) {
                try {
                  await c.setNotifyValue(true);
                  notifyDatas2[c.uuid.toString()] = List.empty();
                  c.value.listen((value) {
                    //print(value.last);
                    //print('${c.uuid}: $value daeng');
                    if (!mounted) return;
                    setState(() {
                      int sum = 0;
                      for (int var_Data in value) {
                        sum += var_Data;
                      }
                      bodyHead = sum.toString();
                      notifyDatas2[c.uuid.toString()] = value;
                      if (StopDeviceHead == 1) {
                        device.disconnect();
                        HeadToSend = sum;
                        print(HeadToSend);
                      }
                      counter++;
                    });
                  });

                  // 설정 후 일정시간 지연
                  await Future.delayed(const Duration(milliseconds: 500));
                } catch (e) {
                  print('error ${c.uuid} $e');
                }
              }
            }
          }
        }
        returnValue = Future.value(true);
      }
    });

    return returnValue ?? Future.value(false);
  }

  Widget listItem(ScanResult r) {
    return ListTile(
      onTap: () => onTap(r),
      leading: leading(r),
      title: deviceName(r),
      subtitle: deviceMacAddress(r),
      trailing: deviceSignal(r),
    );
  }

  Future<void> changeBodyHeight() async {
    // scan("08:B6:1F:3B:57:2A");
    Navigator.pop(context);
    int min = 130;
    int max = 195;
    int randomNum = ((Random().nextInt((max + 5) ~/ 5) * 5) + min);
    if (!mounted) return;
    setState(() {
      bodyHeight = randomNum.toString();
    });
  }

  Stream _bluetoothList() async* {
    await Future<void>.delayed(const Duration(seconds: 3));
  }

  void changeBodyWeight() {
    // scan("08:B6:1F:34:86:4E");
    Navigator.pop(context);
    int min = 35;
    int max = 95;
    int randomNum = ((Random().nextInt((max + 5) ~/ 5) * 5) + min);
    if (!mounted) return;
    setState(() {
      bodyWeight = randomNum.toString();
    });
  }

  void changeBodyHead() {
    // scan("94:B5:55:2E:45:E6");
    Navigator.pop(context);
    int min = 15;
    int max = 75;
    int randomNum = ((Random().nextInt((max + 5) ~/ 5) * 5) + min);
    if (!mounted) return;
    setState(() {
      bodyHead = randomNum.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Anak - Anak',
          style: SafeGoogleFont(
            'Nunito',
            fontWeight: FontWeight.w700,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: <Color>[Color(0xfff6f6f6), Color(0xff7fcce2)],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: double.infinity,
                padding: EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      // frame127Rpc (43:1623)
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 0 * fem, 23 * fem),
                      width: 360 * fem,
                      height: 339 * fem,
                      child: Image.asset(
                        'assets/page-1/images/anak.png',
                        width: 360 * fem,
                        height: 339 * fem,
                      ),
                    ),
                    Container(
                      // frame133jqJ (43:1737)
                      margin: EdgeInsets.fromLTRB(
                          1 * fem, 0 * fem, 0 * fem, 0 * fem),
                      width: 359 * fem,
                      child: Column(
                        children: [
                          Container(
                            // frame131U2C (43:1731)
                            padding: EdgeInsets.fromLTRB(
                                6 * fem, 5 * fem, 17.75 * fem, 5 * fem),
                            width: double.infinity,
                            height: 80 * fem,
                            decoration: BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(12 * fem),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x0a000000),
                                  offset: Offset(0 * fem, 4 * fem),
                                  blurRadius: 8.5 * fem,
                                ),
                              ],
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  width: 65.07 * fem,
                                  height: 70 * fem,
                                  child: Image.asset(
                                    'assets/page-1/images/frame-129-4bz.png',
                                    width: 65.07 * fem,
                                    height: 70 * fem,
                                  ),
                                ),
                                const SizedBox(width: 10),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      child: Text(
                                        'height',
                                        style: SafeGoogleFont(
                                          'Nunito',
                                          fontSize: 20 * ffem,
                                          fontWeight: FontWeight.w600,
                                          height: 1.3625 * ffem / fem,
                                          color: Color(0xff9496a1),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      child: Text(
                                        bodyHeight,
                                        style: SafeGoogleFont(
                                          'Nunito',
                                          fontSize: 20 * ffem,
                                          fontWeight: FontWeight.w600,
                                          height: 1.3625 * ffem / fem,
                                          color: Color(0xff000000),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Spacer(),
                                ElevatedButton(
                                  onPressed: () {
                                    if (StopDeviceHeight == 0) {
                                      IScan1(macData.get("macWeight"));
                                    }
                                    StopDeviceHeight = 1;
                                  },
                                  child: const Text(
                                    'Save',
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 8 * fem,
                          ),
                          Container(
                            // frame1325Jp (43:1738)
                            padding: EdgeInsets.fromLTRB(
                                6 * fem, 5 * fem, 17.75 * fem, 5 * fem),
                            width: double.infinity,
                            height: 80 * fem,
                            decoration: BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(12 * fem),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x0a000000),
                                  offset: Offset(0 * fem, 4 * fem),
                                  blurRadius: 8.5 * fem,
                                ),
                              ],
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  width: 65.07 * fem,
                                  height: 70 * fem,
                                  child: Image.asset(
                                    'assets/page-1/images/frame-129.png',
                                    width: 65.07 * fem,
                                    height: 70 * fem,
                                  ),
                                ),
                                const SizedBox(width: 10),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      child: Text(
                                        'weight',
                                        style: SafeGoogleFont(
                                          'Nunito',
                                          fontSize: 20 * ffem,
                                          fontWeight: FontWeight.w600,
                                          height: 1.3625 * ffem / fem,
                                          color: Color(0xff9496a1),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      child: Text(
                                        bodyWeight,
                                        style: SafeGoogleFont(
                                          'Nunito',
                                          fontSize: 20 * ffem,
                                          fontWeight: FontWeight.w600,
                                          height: 1.3625 * ffem / fem,
                                          color: Color(0xff000000),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Spacer(),
                                StreamBuilder(
                                  //Error number 2
                                  stream: NumberCreator().stream,
                                  builder: (context, snapshot) {
                                    if (StopDeviceHeight == 1) {
                                      return ElevatedButton(
                                        onPressed: () {
                                          if (StopDeviceWeight == 0) {
                                            IScan2(macData.get("macHead"));
                                          }
                                          StopDeviceWeight = 1;
                                        },
                                        child: const Text(
                                          'Save',
                                          style: TextStyle(fontSize: 12),
                                        ),
                                      );
                                    } else {
                                      return Text(
                                        '-',
                                        style: SafeGoogleFont(
                                          'Nunito',
                                          fontSize: 20 * ffem,
                                          fontWeight: FontWeight.w600,
                                          height: 1.3625 * ffem / fem,
                                          color: Color(0xff9496a1),
                                        ),
                                      );
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 8 * fem,
                          ),
                          Container(
                            // frame133fpk (43:1743)
                            padding: EdgeInsets.fromLTRB(
                                6 * fem, 5 * fem, 17.75 * fem, 5 * fem),
                            width: double.infinity,
                            height: 80 * fem,
                            decoration: BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(12 * fem),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x0a000000),
                                  offset: Offset(0 * fem, 4 * fem),
                                  blurRadius: 8.5 * fem,
                                ),
                              ],
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  width: 65.07 * fem,
                                  height: 70 * fem,
                                  child: Image.asset(
                                    'assets/page-1/images/frame-129-tsA.png',
                                    width: 65.07 * fem,
                                    height: 70 * fem,
                                  ),
                                ),
                                const SizedBox(width: 10),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      child: Text(
                                        'head',
                                        style: SafeGoogleFont(
                                          'Nunito',
                                          fontSize: 20 * ffem,
                                          fontWeight: FontWeight.w600,
                                          height: 1.3625 * ffem / fem,
                                          color: Color(0xff9496a1),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      child: Text(
                                        bodyHead,
                                        style: SafeGoogleFont(
                                          'Nunito',
                                          fontSize: 20 * ffem,
                                          fontWeight: FontWeight.w600,
                                          height: 1.3625 * ffem / fem,
                                          color: Color(0xff000000),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Spacer(),
                                StreamBuilder(
                                  //Error number 2
                                  stream: NumberCreator().stream,
                                  builder: (context, snapshot) {
                                    if (StopDeviceWeight == 1) {
                                      return ElevatedButton(
                                        onPressed: () {
                                          StopDeviceHead = 1;
                                        },
                                        child: const Text(
                                          'Save',
                                          style: TextStyle(fontSize: 12),
                                        ),
                                      );
                                    } else {
                                      return Text(
                                        '-',
                                        style: SafeGoogleFont(
                                          'Nunito',
                                          fontSize: 20 * ffem,
                                          fontWeight: FontWeight.w600,
                                          height: 1.3625 * ffem / fem,
                                          color: Color(0xff9496a1),
                                        ),
                                      );
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(12.0),
                      width: double.maxFinite,
                      child: ElevatedButton(
                        onPressed: () => onSubmit(),
                        child: Center(
                          child: Text('Submit'),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
