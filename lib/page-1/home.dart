import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:hive/hive.dart';
import 'package:myapp/page-1/setting.dart';
import 'package:myapp/utils.dart';
import 'nik_screen.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Home();
  }
}

class _Home extends State<Home> {
  final _userData = Hive.box("userData");
  String name = "";

  void onStartClick() {
    context.goNamed('category');
  }

  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Home',
          style: SafeGoogleFont('Nunito',
              fontWeight: FontWeight.w900, color: Color(0xff50b848)),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: Container(
          width: double.infinity,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomRight,
              colors: <Color>[Color(0xfff6f6f6), Color(0xff7fcce2)],
              stops: <double>[0, 1],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                // autogroupd5teEsi (6aqPyUBs3Hi4pegTiEd5Te)
                padding: EdgeInsets.all(12.0),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      // welcomeukanstaywellAWU (5:1083)
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 57 * fem, 4 * fem),
                      child: Text(
                        "Welcome ${_userData.get('name')}, Stay Well",
                        style: SafeGoogleFont(
                          'Nunito',
                          fontSize: 24 * ffem,
                          fontWeight: FontWeight.w800,
                          height: 1.3625 * ffem / fem,
                          color: const Color(0xff50b848),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 2 * fem, 30 * fem),
                      constraints: BoxConstraints(
                        maxWidth: 357 * fem,
                      ),
                      child: Text(
                        'Semoga Anda Tetap Sehat dan Selalu Jaga Kesehatan Ya :)',
                        style: SafeGoogleFont(
                          'Nunito',
                          fontSize: 14 * ffem,
                          fontWeight: FontWeight.w500,
                          height: 1.3625 * ffem / fem,
                          color: const Color(0xff9496a1),
                        ),
                      ),
                    ),
                    SizedBox(height: 50),
                    Container(
                      height: 70 * ffem,
                      child: AspectRatio(
                        aspectRatio: 16 / 4,
                        child: Image.asset(
                          'assets/page-1/images/IndoPrima.png',
                        ),
                      ),
                    ),
                    SizedBox(height: 100),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 2 * fem, 30 * fem),
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () => onStartClick(),
                            child: Card(
                              color: Color.fromARGB(255, 79, 184, 72),
                              elevation: 10,
                              child: Container(
                                width: double.infinity,
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                        // akariconsnewspaperAAL (43:1769)
                                        margin: EdgeInsets.fromLTRB(0 * fem,
                                            0 * fem, 0 * fem, 16 * fem),
                                        width: 50 * fem,
                                        height: 50 * fem,
                                        child: Card(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(80.0)),
                                          color: Colors.white,
                                          child: Icon(
                                              CupertinoIcons.barcode_viewfinder,
                                              color: Colors.blue,
                                              size: 26.0),
                                        )),
                                    Container(
                                      // hometreatment4Fi (5:1067)
                                      margin: EdgeInsets.fromLTRB(
                                          0 * fem, 0 * fem, 0 * fem, 5 * fem),
                                      child: Text(
                                        'Scan',
                                        style: SafeGoogleFont(
                                          'Nunito',
                                          fontSize: 14 * ffem,
                                          fontWeight: FontWeight.w700,
                                          height: 1.3625 * ffem / fem,
                                          color: Color.fromARGB(
                                              255, 255, 255, 255),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      // hometreatment4Fi (5:1067)
                                      margin: EdgeInsets.fromLTRB(
                                          0 * fem, 0 * fem, 0 * fem, 9 * fem),
                                      child: Text(
                                        'Scan Data Pasien Sekarang',
                                        style: SafeGoogleFont(
                                          'Nunito',
                                          fontSize: 14 * ffem,
                                          fontWeight: FontWeight.w700,
                                          height: 1.3625 * ffem / fem,
                                          color: Color.fromARGB(
                                              255, 255, 255, 255),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () => context.goNamed('setting'),
                            child: Card(
                              color: Colors.lightBlue,
                              elevation: 10,
                              child: Container(
                                width: double.infinity,
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                        // akariconsnewspaperAAL (43:1769)
                                        margin: EdgeInsets.fromLTRB(0 * fem,
                                            0 * fem, 0 * fem, 16 * fem),
                                        width: 50 * fem,
                                        height: 50 * fem,
                                        child: Card(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(80.0)),
                                          color: Colors.white,
                                          child: Icon(CupertinoIcons.gear,
                                              color: Colors.blue, size: 26.0),
                                        )),
                                    Container(
                                      // hometreatment4Fi (5:1067)
                                      margin: EdgeInsets.fromLTRB(
                                          0 * fem, 0 * fem, 0 * fem, 5 * fem),
                                      child: Text(
                                        'Setting',
                                        style: SafeGoogleFont(
                                          'Nunito',
                                          fontSize: 14 * ffem,
                                          fontWeight: FontWeight.w700,
                                          height: 1.3625 * ffem / fem,
                                          color: Color.fromARGB(
                                              255, 255, 255, 255),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      // hometreatment4Fi (5:1067)
                                      margin: EdgeInsets.fromLTRB(
                                          0 * fem, 0 * fem, 0 * fem, 9 * fem),
                                      child: Text(
                                        'Scan Data Pasien Sekarang',
                                        style: SafeGoogleFont(
                                          'Nunito',
                                          fontSize: 14 * ffem,
                                          fontWeight: FontWeight.w700,
                                          height: 1.3625 * ffem / fem,
                                          color: Color.fromARGB(
                                              255, 255, 255, 255),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
