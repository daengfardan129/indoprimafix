import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:myapp/page-1/body_measurements.dart';
import 'package:myapp/page-1/setting.dart';
import 'package:myapp/page-1/setting_child.dart';
import 'package:myapp/utils.dart';

class SettingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SettingScreen();
  }
}

class _SettingScreen extends State<SettingScreen> {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Setting',
            style: SafeGoogleFont('Nunito', fontWeight: FontWeight.w700),
          ),
        ),
        body: Center(
          child: Container(
            width: double.infinity,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: <Color>[Color(0xfff6f6f6), Color(0xff7fcce2)],
                stops: <double>[0, 1],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  // autogroupd5teEsi (6aqPyUBs3Hi4pegTiEd5Te)
                  padding: EdgeInsets.fromLTRB(
                      34 * fem, 50 * fem, 33 * fem, 59 * fem),
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        // welcomeukanstaywellAWU (5:1083)
                        margin: EdgeInsets.fromLTRB(
                            0 * fem, 0 * fem, 57 * fem, 4 * fem),
                        child: Text(
                          'Pilih Usia Pasien',
                          style: SafeGoogleFont(
                            'Nunito',
                            fontSize: 18 * ffem,
                            fontWeight: FontWeight.w800,
                            height: 1.3625 * ffem / fem,
                            color: Color.fromARGB(255, 24, 65, 109),
                          ),
                        ),
                      ),
                      Container(
                        // loremipsumdolorsitametconsecte (5:1084)
                        margin: EdgeInsets.fromLTRB(
                            0 * fem, 0 * fem, 2 * fem, 30 * fem),
                        constraints: BoxConstraints(
                          maxWidth: 357 * fem,
                        ),
                        child: Text(
                          'Pilih Menu Sesuai Dengan Umur Pasien',
                          style: SafeGoogleFont(
                            'Nunito',
                            fontSize: 14 * ffem,
                            fontWeight: FontWeight.w500,
                            height: 1.3625 * ffem / fem,
                            color: const Color(0xff9496a1),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(
                            1 * fem, 0 * fem, 0 * fem, 0 * fem),
                        child: SizedBox(
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () => context.goNamed('settingAdult'),
                                child: Card(
                                  color: Color.fromARGB(255, 79, 184, 72),
                                  elevation: 10,
                                  child: Container(
                                    padding: EdgeInsets.all(10.0),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                            width: 50 * fem,
                                            height: 50 * fem,
                                            child: Card(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          80.0)),
                                              color: Colors.white,
                                              child: Icon(
                                                  CupertinoIcons.person_alt,
                                                  color: Colors.blue,
                                                  size: 26.0),
                                            )),
                                        Container(
                                          // hometreatment4Fi (5:1067)
                                          margin: EdgeInsets.fromLTRB(0 * fem,
                                              0 * fem, 0 * fem, 5 * fem),
                                          child: Text(
                                            'Dewasa',
                                            style: SafeGoogleFont(
                                              'Nunito',
                                              fontSize: 14 * ffem,
                                              fontWeight: FontWeight.w700,
                                              height: 1.3625 * ffem / fem,
                                              color: Color.fromARGB(
                                                  255, 255, 255, 255),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () => context.goNamed('settingChild'),
                                child: Card(
                                  color: Colors.lightBlue,
                                  elevation: 10,
                                  child: Container(
                                    padding: EdgeInsets.all(10.0),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                            width: 50 * fem,
                                            height: 50 * fem,
                                            child: Card(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          80.0)),
                                              color: Colors.white,
                                              child: Icon(
                                                  CupertinoIcons.smiley_fill,
                                                  color: Colors.blue,
                                                  size: 26.0),
                                            )),
                                        Container(
                                          // hometreatment4Fi (5:1067)
                                          margin: EdgeInsets.fromLTRB(0 * fem,
                                              0 * fem, 0 * fem, 5 * fem),
                                          child: Text(
                                            'Anak',
                                            style: SafeGoogleFont(
                                              'Nunito',
                                              fontSize: 14 * ffem,
                                              fontWeight: FontWeight.w700,
                                              height: 1.3625 * ffem / fem,
                                              color: Color.fromARGB(
                                                  255, 255, 255, 255),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
