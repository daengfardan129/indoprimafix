import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

class ApiService {
  final String baseUrl = 'http://178.128.91.198';
  final String pathLogin = '/web/session/authenticate';
  final String patientData = '/api/v1/patient_data';

  Future login(formData) async {
    print("======= start LoginApi =======");
    try {
      var url = Uri.parse(baseUrl + pathLogin);
      http.Response response = await http.post(url,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(formData));
      print("status Code ${response.statusCode}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return response.body;
      }
    } catch (e) {
      log(e.toString());
    }
    print("======= end LoginApi =======");
  }

  Future insertPatientData(formData) async {
    print("======= start insertPatientData =======");
    try {
      var url = Uri.parse(baseUrl + patientData);
      http.Response response = await http.post(url,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(formData));
      print("status Code ${response.statusCode}");
      if (response.statusCode == 200) {
        return response.body;
      } else {
        return response.body;
      }
    } catch (e) {
      log(e.toString());
    }
    print("======= end insertPatientData =======");
  }
}
