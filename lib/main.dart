import 'dart:math';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:myapp/page-1/body_measurements.dart';
import 'package:myapp/page-1/body_measurements_child.dart';
import 'package:myapp/page-1/category_screen.dart';
import 'package:myapp/page-1/home.dart';
import 'package:myapp/page-1/login.dart';
import 'package:myapp/page-1/nik_screen.dart';
import 'package:myapp/page-1/setting.dart';
import 'package:myapp/page-1/setting_child.dart';
import 'package:myapp/page-1/setting_screen.dart';
import 'package:myapp/utils.dart';

import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final applicatonDocumentDir =
      await path_provider.getApplicationDocumentsDirectory();
  print("aplikasi dir");
  print(applicatonDocumentDir);
  Hive.init(applicatonDocumentDir.path);

  var userData = await Hive.openBox("userData");
  var patienData = await Hive.openBox("patienData");
  var macDataDewasa = await Hive.openBox("macDataDewasa");
  var macDataAnak = await Hive.openBox("macDataAnak");
  // print(macData);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyApp();
}

class _MyApp extends State<MyApp> {
  final _router = GoRouter(
    initialLocation: '/',
    routes: [
      GoRoute(
        path: '/',
        name: 'login',
        builder: (context, state) => Login(),
      ),
      GoRoute(
        path: '/home',
        name: 'home',
        builder: (context, state) {
          return Home();
        },
        routes: [
          GoRoute(
              path: 'category',
              name: 'category',
              builder: (context, state) => CategoryScreen(),
              routes: [
                GoRoute(
                  path: 'body-measurement',
                  name: 'bodyMeasurement',
                  builder: (context, state) {
                    return BodyMeasurements();
                  },
                ),
                GoRoute(
                  path: 'body-measurement-child',
                  name: 'bodyMeasurementChild',
                  builder: (context, state) {
                    return BodyMeasurementsChild();
                  },
                ),
              ]),
          GoRoute(
            path: 'patien-data',
            name: 'patienData',
            builder: (context, state) => NikScreen(),
          ),
          GoRoute(
              path: 'setting',
              name: 'setting',
              builder: (context, state) => SettingScreen(),
              routes: [
                GoRoute(
                    path: 'adult',
                    name: 'settingAdult',
                    builder: (context, state) => Setting()),
                GoRoute(
                    path: 'child',
                    name: 'settingChild',
                    builder: (context, state) => SettingChild())
              ]),
        ],
      ),
    ],
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
        title: 'Flutter',
        debugShowCheckedModeBanner: false,
        scrollBehavior: MyCustomScrollBehavior(),
        theme: ThemeData(primarySwatch: Colors.blue, useMaterial3: true),
        routerConfig: _router);
  }
}
